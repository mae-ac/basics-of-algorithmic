Let's get back to the _Fibonacci_ sequence.

Below is a reminder of its definition.
- $F_0 = 0$
- $F_1 = 1$
- $\forall n \geq 2, \quad F_n = F_{n - 1} + F_{n - 2}$

### Q5. [[toc](README.md#table-of-content)]
:pencil: Compute the complexity of the _recursive_ version from [Q1](README.md#q1-toc).
Write your findings in a comment just above the function.

In your comment, you should answer the questions from the method from [_How to compute complexities in practice_](complexity_families_and_practice.md#how-to-compute-complexities-in-practice-toc):
- what should count in the complexity
- compute the number of such operations
- apply the $O$ operator

> :bulb: **Hint**
>
> the _Fibonacci_ numbers also have a closed-form expression... :smirk:
>
> $$F_n = \frac{\phi^n - \psi^n}{\phi - \psi}$$
> where $\phi = 1 - \psi = \frac{1 + \sqrt{5}}{2}$

### Q6. [[toc](README.md#table-of-content)]
:pencil: Compute the complexities of the _memoized_ and _iterative_ versions from
[Q3](README.md#q3-toc) and [Q4](README.md#q4-toc).
Write your findings in comments just above the appropriate functions.

In your comment, you should answer once again the questions from the method from
[_How to compute complexities in practice_](complexity_families_and_practice.md#how-to-compute-complexities-in-practice-toc):
- what should count in the complexity
- compute the number of such operations
- apply the $O$ operator

### Q7. [[toc](README.md#table-of-content)]
Do you understand now why the _recursive_ version is **much** slower?

:file_folder: Create a new file called `src/fibo_time.py` plot your results from there.

:pencil: Confirm that intuition by measuring the execution time of your _Fibonacci_
functions for several input values $n$ and then plotting the results with
`matplotlib`.

:gear: Make sure the command `python src/fibo_time.py` shows the correct plot.

---
---
> :point_right: [NEXT](sorting.md)
