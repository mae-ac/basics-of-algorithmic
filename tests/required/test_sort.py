from random import shuffle
from typing import Any, Callable, List
from copy import deepcopy


def __test_sort_immutable(
    f: Callable[[List[Any]], List[Any]],
    l: List[Any],
    expected: List[Any],
    msg: str,
):
    before = deepcopy(l)
    actual = f(l)
    assert before == l, "sort should not mutate input list"
    assert actual == expected, msg


def __test_sort(f: Callable[[List[Any]], List[Any]], n: int):
    l_to_sort = list(range(n))
    sorted = list(range(n))

    __test_sort_immutable(f, l_to_sort, sorted, "best case should work")
    __test_sort_immutable(f, list(reversed(l_to_sort)), sorted, "worst case should work")

    shuffle(l_to_sort)
    __test_sort_immutable(f, l_to_sort, sorted, "average case should work")


def test_bubble():
    from src.sort import bubble_sort
    for n in range(20):
        __test_sort(bubble_sort, n)


def test_selection():
    from src.sort import selection_sort
    for n in range(20):
        __test_sort(selection_sort, n)


def test_quick_sort():
    from src.sort import quick_sort
    for n in range(20):
        __test_sort(quick_sort, n)


def test_merge_sort():
    from src.sort import merge_sort
    for n in range(20):
        __test_sort(merge_sort, n)
