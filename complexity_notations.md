### The $O$, $\Theta$ and $\Omega$ notations [[toc](README.md#table-of-content)]

_Asymptotic_ complexities might be easier to compute and manipulate that the
exact number of operations or the exact memory footprint.

Let's say the exact number of operations or memory is $f(n)$, where $n$ is the
size of the input of the algorithm.

> The next sections have been inspired by
> [_Big Oh(O) vs Big Omega(Ω) vs Big Theta(θ) notations | Asymptotic Analysis of Algorithms with Example_](https://youtu.be/1tfdr1Iv6JA)

#### Upper bounds and $O$
We can express any _upper bound_ $g(n)$ on the exact complexity $f(n)$ with the
following notation

$$f(n) = O(g(n))$$

This can be associated with the _worst case_ scenario.

> :bulb: **Note**
>
> the formalism between the $O$ notation is the following:
>
> for any $f(n)$ and $g(n)$, we say that $f(n) = O(g(n))$ iif there exists a
> constant $c \gt 0$, i.e. that does not depend on $n$, and an integer
> $n_0 \geq 1$ such that,
>
> $$\forall n \geq n_0, \quad f(n) \leq cg(n)$$

#### Lower bounds and $\Omega$
We can express any _lower bound_ $g(n)$ on the exact complexity $f(n)$ with the
following notation

$$f(n) = \Omega(g(n))$$

This can be associated with the _best case_ scenario and tells what the fastest
time or smallest memory footprint could ever be.

> :bulb: **Note**
>
> the formalism between the $\Omega$ notation is the following:
>
> for any $f(n)$ and $g(n)$, we say that $f(n) = \Omega(g(n))$ iif there exists
> a constant $c \gt 0$, i.e. that does not depend on $n$, and an integer
> $n_0 \geq 1$ such that,
>
> $$\forall n \geq n_0, \quad f(n) \geq cg(n)$$

#### Best fit and $\Theta$
We can express _good fits_ $g(n)$ on the exact complexity $f(n)$ with the
following notation

$$f(n) = \Theta(g(n))$$

This can be associated with the _average case_ scenario and represents the most
realistic complexity of an algorithm.

> :bulb: **Note**
>
> the formalism between the $\Theta$ notation is the following:
>
> for any $f(n)$ and $g(n)$, we say that $f(n) = \Theta(g(n))$ iif there exists
> two strictly positive constants $c_1$ and $c_2$, i.e. that do not depend on
> $n$, and an integer $n_0 \geq 1$ such that,
>
> $$\forall n \geq n_0, \quad c_1g(n) \leq f(n) \leq c_2g(n)$$

---
---
> :point_right: [NEXT](complexity_families_and_practice.md)
