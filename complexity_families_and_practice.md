### Classic families of algorithms [[toc](README.md#table-of-content)]

Algorithms are generally classified in a few families depending on their
_asymptotic_ complexities

| type          | complexity    |
| ------------- | ------------- |
| _constant_    | $O(1)$        |
| _logarithmic_ | $O(\log n)$   |
| _linear_      | $O(n)$        |
| _quasilinear_ | $O(n \log n)$ |
| _quadratic_   | $O(n^2)$      |
| _polynomial_  | $O(n^k)$      |
| _exponential_ | $O(b^n)$      |
| _factorial_   | $O(n!)$       |

> the table above mostly comes from [_Learn Big O notation in 6 minutes_](https://youtu.be/XMUe3zFhM5c)

### How to compute complexities in practice [[toc](README.md#table-of-content)]

First of all, you will need to determine what is worth being accounted for.

For time complexities, are all operations similar? Should the additions or
multiplications be counted? If both are counted, should the multiplication cost
the same as the addition? 10 times more? Should the copying of data count? ...

For space complexities, should all the bytes be counted or only more abstract
things such as "_array elements_"?

The answers to these questions can change the final complexity and there is
probably not a single right answer...
However, there are some general guidelines that are commonly used
- for time complexities, it is common to count only the most costly operations /
  the operation that represents the best the algorithm, e.g. in the case of
  matrix multiplication, we might only count the multiplication and consider the
  addition negligible and in the case of sorting lists, we might only count the
  number of comparisons and discard the swaps of elements.
- for space complexities, we generally simply count the number of _things_ in
  the data structures, i.e. the length of a list or the number of nodes or
  leaves in a tree

Once the set of things we want to consider has been identified, we can start
counting all the operations in the algorithm:
- if there are loops, things will generally multiply
- if there are recursive calls, things will generally involve inductive proofs
- ...

Once we have the exact complexity, it can be transformed into its _asymptotic_
version by discarding all terms and all constants apart from the term that
dominates all the others.

#### An example
Let's take a very simple example to illustrate the method.

```python
def f(n: int) -> int:
    v = 0
    for i in range(n):
        for j in range(i + 1, n):
            v += i * j
    return v
```

which will generate the following values:
| $n$    | 0 | 1 | 2 | 3 | 4  | 5  | 6  | 7   | 8   | 9   |
| ------ | - | - | - | - | -- | -- | -- | --- | --- | --- |
| $f(n)$ | 0 | 0 | 0 | 2 | 11 | 35 | 85 | 175 | 322 | 546 |

We will follow the method:
- determine what to count: only multiplications between integers will be
  accounted for, additions between integers, variable assignments and iterating
  over _ranges_ will be discarded.
- count the number of operations: the only multiplication is inside the inner
  `for` loop. The first iteration of the outter `for` loop will cause the inner
  one to iterate from `i + 1` to `n` exclusive, i.e. from `1` to `n - 1` and
  that counts for $n - 1$ multiplication. On the second iteration of the outter
  loop, `i` is equal to `1` and thus the inner loop counts $n - 2$
  multiplications. That continues until `i` is `0` and the inner loop counts no
  multiplication. The total number of multiplication is thus
  $$(n - 1) + (n - 2) + ... + 1 + 0 = \frac{n(n - 1)}{2} = \frac{1}{2}n^2 - \frac{1}{2}n$$
- finally, we can apply the $O$ operator to the number of operations and get the
  _asymptotic_ complexity $c_f(n)$
  $$c_f(n) = O(\frac{1}{2}n^2 - \frac{1}{2}n) = O(n^2)$$

Thus, $f$ is a _quadratic_ algorithm.

### The Master Theorem [[toc](README.md#table-of-content)]

> :bulb: **Note**
>
> This section applies to a family of algorithms that we haven't talked about yet.
>
> As it is quite a lot more technical that the rest, feel free to skip it
> for now and come back to it when required :)

There is a paradigm called _Divide and Conquer_ which relies heavily on
_recursion_ and helps solve problems by _dividing_ them into smaller subproblems
and then _conquering_ these subproblems.

The general form of a _Divide and Conquer_ algorithm on an input of size $n$ is
the following:
- each problem is divided into $a$ subproblems that are a factor $b$ smaller,
  i.e. of size $\frac{n}{b}$
- once all subproblems have been solved, their solutions are merged back
  together using an algorithm whose complexity is $f(n)$

which leads us to
$$T(n) = aT(\frac{n}{b}) + f(n)$$
for constants $a \geq 1$ and $b \gt 1$, and let $\epsilon \gt 0$

Then the _Master Theorem_ gives us the complexity $T(n)$ depending on what the
complexity of $f(n)$ is compared to the complexity of $n^{\log_ba}$
| case | $f(n)$                           | $f(n)$ compared to $n^{\log_ba}$ | $T(n)$                      |
| ---- | -------------------------------- | -------------------------------- | --------------------------- |
| 1    | $O(n^{\log_b(a)-\epsilon})$      | polynomially smaller             | $\Theta(n^{\log_ba})$       |
| 2    | $\Theta(n^{\log_ba})$            | asymptotically the same          | $\Theta(n^{\log_ba}\log n)$ |
| 3    | $\Omega(n^{\log_b(a)+\epsilon})$ | polynomially larger              | $\Theta(f(n))$              |

> inspired by [_Master Theorem_](https://brilliant.org/wiki/master-theorem/)

---
---
> :point_right: [NEXT](complexity_fibonacci.md)
